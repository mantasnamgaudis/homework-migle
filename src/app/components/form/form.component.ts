import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailService } from '../../service/email.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  exampleForm: FormGroup;
  isSubmitted: boolean;
  isFriendCountInvalid: boolean;
  result: string;

  constructor(private formBuilder: FormBuilder, private emailService: EmailService) {
  }

  ngOnInit(): void {
    this.exampleForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
      friends: new FormArray([]),
    });
  }


  submit(): void {
    this.isSubmitted = true;
    this.checkEmail();
    this.checkPasswordConfirm();
    this.checkFriends();
    if (this.exampleForm.valid && !this.isFriendCountInvalid) {
      this.result = JSON.stringify(this.exampleForm.value);
    } else {
      this.exampleForm.updateValueAndValidity();
    }
  }

  isInputValid(input: string): boolean {
    return this.isSubmitted && this.exampleForm.get(input).invalid;
  }

  isFriendInputValid(input: string, index: number): boolean {
    return this.isSubmitted && this.friends.controls[index].get(input).invalid;
  }

  checkEmail(): void {
    if (this.emailService.isExistingEmail(this.email.value)) {
      this.setError(this.email, 'isExistingEmail', true);
    } else {
      this.setError(this.email, 'isExistingEmail', false);
    }
  }

  checkPasswordConfirm(): void {
    if (this.passwordConfirm.value.length > 0 && this.password.value !== this.passwordConfirm.value) {
      this.setError(this.passwordConfirm, 'noMatch', true);
    } else {
      this.setError(this.passwordConfirm, 'noMatch', false);
    }
  }

  checkFriends(): void {
    this.isFriendCountInvalid = this.friends.length < 1;
  }

  addFriend(): void {
    this.friends.push(this.formBuilder.group({
      friendName: ['', Validators.required],
      friendEmail: ['', [Validators.required, Validators.email]],
    }));
    this.isFriendCountInvalid = false;
  }

  setError(input: AbstractControl, key: string, value): void {
    const errors = input.errors;
    if (!value && errors && errors[key]) {
      delete errors[key];
      input.setErrors(errors);
    } else if (value) {
      input.setErrors({ ...errors, [key]: value });
    }
  }

  get name(): AbstractControl {
    return this.exampleForm.get('name');
  }

  get email(): AbstractControl {
    return this.exampleForm.get('email');
  }

  get password(): AbstractControl {
    return this.exampleForm.get('password');
  }

  get passwordConfirm(): AbstractControl {
    return this.exampleForm.get('passwordConfirm');
  }

  get friends(): FormArray {
    return this.exampleForm.get('friends') as FormArray;
  }


}
